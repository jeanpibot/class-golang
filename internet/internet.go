package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
)

//SitemapIndex is for blablablabla
type SitemapIndex struct {
	Locations []Location `xml:"sitemap"`
}

//Location is a slice
type Location struct {
	Loc string `xml:"loc"`
}

func main() {
	response, _ := http.Get("https://www.washingtonpost.com/news-sitemap-index.xml")
	bytes, _ := ioutil.ReadAll(response.Body)
	response.Body.Close()

	var s SitemapIndex
	xml.Unmarshal(bytes, &s)

	fmt.Println(s.Locations)

}
