package main

import "fmt"

func main()  {

	//First way initialize arrays
	var firstArray [2]int

	firstArray[0] = 2
	firstArray[1] = 3

	fmt.Println(firstArray)

	//len is use to get the length of the array
	fmt.Println(len(firstArray))

	//Initialize the array whit values
	scores := [4]int {9001,9302,3920,1231}

	fmt.Println(scores)

	//range can be use to iterate over it
	for index, value := range scores {
		fmt.Printf("el index es %d y el valor es %d\n", index, value)
	}
}